--------------------------------------------------------------------------------
-- Engineer: Klimann Wendlin
--
-- Create Date:   21:29:17 04/Jun/2014
-- Design Name:   lt2378_tb
-- Description:   
-- 
-- VHDL Test Bench for module: lt2378
--
--
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

ENTITY ltc2378_tb_vhd IS
END ltc2378_tb_vhd;

ARCHITECTURE behavior OF ltc2378_tb_vhd IS 
	constant width : integer := 18;
	-- Component Declaration for the Unit Under Test (UUT)
	COMPONENT ltc2378
	--generic(width : integer := width);
	PORT(
	CNV_in      : in  std_logic;      -- Convert In Signal
	CLK_50MHz   : in  std_logic;      -- 50MHz Clock
	DIN         : in  std_logic;      -- Data Input
	BUSY        : in  std_logic;      -- ADC Busy signal
	CNV_out     : out std_logic;      -- Convert Out Signal
	SCK_out     : out std_logic;     
	RESET       : in  std_logic;      -- Asynchronous Reset (Active Low)
	DATA        : out std_logic_vector(23 downto 0);
	DATA_RDY    : out std_logic     --Falling edge means data is ready	
		);
	END COMPONENT;

	--Inputs
	SIGNAL CNV_in      :  std_logic := '0';
	SIGNAL CLK_50MHz   :  std_logic := '0';
	SIGNAL DIN         :  std_logic := '0';
	SIGNAL RESET       :  std_logic := '0';
	SIGNAL BUSY        :  std_logic := '0';  

	--Outputs
	SIGNAL DATA         :  std_logic_vector(23 downto 0);
	SIGNAL CNV_out      :  std_logic;
	SIGNAL DATA_RDY     :  std_logic;
	SIGNAL SCK_out      :  std_logic;

BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut: ltc2378 
	PORT MAP(
	CNV_in      =>    CNV_in,
	CLK_50MHz   =>    CLK_50MHz,
	DIN         =>    DIN,
	BUSY        =>    BUSY,
	CNV_out     =>    CNV_out,
	RESET       =>    RESET,
	DATA        =>    DATA,
	DATA_RDY    =>    DATA_RDY,
	SCK_out     =>    SCK_out
	);
	
	
	p_reset : process
	begin
		RESET <= '1';
		--LR_CK <= '1';
		wait for 640 ns;
		RESET <= '0';
		-- Reset finished
		wait;
	end process	p_reset;
	
	p_bit_clk : process
	begin
		CLK_50MHz <= '0';
		wait for 10 ns;
		CLK_50MHz <= '1';
		wait for 10 ns;
	end process p_bit_clk;
	
	p_busy : process
	begin
		wait for 5 ns;
		BUSY <= '0';
		wait for 420 ns;
		BUSY <= '1';
		wait for 80 ns;
	end process p_busy;
	
	p_cnv : process
	begin
		CNV_in <= '0';
		wait for 420 ns;
		CNV_in <= '1';
		wait for 80 ns;
	end process p_cnv;
	
	p_din : process
	variable i : POSITIVE :=1;
	begin
		wait for 10 ns;
		i := 1;
		loop_1: while i <= 24 loop 
			DIN <= '0';
			wait for 20 ns;
			DIN <= '1';
			wait for 20 ns;
			i := i+1;
		end loop loop_1;
		i := 1;
		loop_2: while i <= 12 loop 
			DIN <= '0';
			wait for 40 ns;
			DIN <= '1';
			wait for 40 ns;
			i := i+1;
		end loop loop_2;
		i := 1;
		loop_3: while i <= 6 loop 
			DIN <= '0';
			wait for 80 ns;
			DIN <= '1';
			wait for 80 ns;
			i := i+1;
		end loop loop_3;
		
	end process p_din;

END;
