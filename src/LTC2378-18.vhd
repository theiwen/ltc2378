---------------------------------------------------------------------------------
-- Engineer:      Klimann Wendelin 
--
-- Create Date:   20:45:11 04/Jun/2014
-- Design Name:   ltc2378-18
--
-- Rev.: 1.1      (10.July.2015)
-- Rev.: 1.2      (13.Sept.2015)
--
-- Description:   
-- 
-- This module provides a bridge between an I2S serial device (audio ADC, S/PDIF 
-- Decoded data) and a parallel device (microcontroller, IP block).
--
-- It's coded as a generic VHDL entity, so developer can choose the proper signal
-- width (8/16/24/32 bit)
--
-- Input takes:
-- -CNV_in
-- -CLK_50Mhz (should be set to about a min. frequ of 50MHz)
-- -DIN
-- -BUSY
--
-- Output provides:
-- -DATA
-- -DATA_RDY output ready signal.
-- -CNV_out
-- -SCK_out
-- 
--
-- The data from the parallel inputs is shifted to the I2S data output
--
--------------------------------------------------------------------------------
--
-- In Rev.:1.1  the DATA_RDY is changed to be always '1' just while the shift_reg
-- is copied to DATA it is set to '0' 
--
--------------------------------------------------------------------------------
--
-- Rev.: 1.2 changes the signal 's_counter' to integer range '0 to width'
-- this was essential to be compilablewith the Xilinx Zynq FPGA 
--
-- Output DATA is changed to be 24 Bit with 
--      Bit 24 -> MSB
--      Bit  6 -> LSB
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity ltc2378 is 
-- width: How many bits (from MSB) are gathered from the serial input
generic(width : integer := 18);

port(
	--  LT2378 ports
	CNV_in      : in  std_logic;      -- Convert In Signal
	CLK_50MHz   : in  std_logic;      -- 50MHz Clock
	DIN         : in  std_logic;      -- Data Input
	BUSY        : in  std_logic;      -- ADC Busy signal
	CNV_out     : out std_logic;      -- Convert Out Signal
	SCK_out     : out std_logic;      -- Clock for the ADC
	
	-- Control ports
	RESET       : in  std_logic;      -- Asynchronous Reset (Active Low)
	
	-- Parallel ports
	DATA        : out std_logic_vector(23 downto 0);
	
	-- Output status ports
	DATA_RDY    : out std_logic     --Falling edge means data is ready
);
end ltc2378;


architecture rtl of ltc2378 is

	--signals 
	signal s_shift_reg       : std_logic_vector(width-1 downto 0);
	signal s_current_busy    : std_logic;
	signal s_clk_enable      : std_logic;
	signal s_rdy             : std_logic;
	signal s_counter_enable  : std_logic;
	signal s_counter         : integer range 0 to width;
	
		
begin

	-- serial to parallel interface
	ltc2378: process(RESET, CLK_50MHz, DIN, BUSY)
	begin
		if(RESET = '1') then
		
			DATA         <= (others => '0');
			s_shift_reg  <= (others => '0');
			
			s_current_busy   <= '0';
			
		elsif(CLK_50MHz'event and CLK_50MHz = '0') then
		
			if(s_counter_enable = '1') then
				s_counter <= s_counter + 1;
				if(s_counter < width) then
					s_shift_reg(s_counter) <= DIN;
					s_clk_enable    <= '1';  
				else
					s_counter    <=  0 ;
					s_clk_enable <= '0';
					DATA(23) <= s_shift_reg(0); 
					DATA(22) <= s_shift_reg(1);
					DATA(21) <= s_shift_reg(2);
					DATA(20) <= s_shift_reg(3);
					DATA(19) <= s_shift_reg(4);
					DATA(18) <= s_shift_reg(5);
					DATA(17) <= s_shift_reg(6);
					DATA(16) <= s_shift_reg(7);
					DATA(15) <= s_shift_reg(8);
					DATA(14) <= s_shift_reg(9);
					DATA(13) <= s_shift_reg(10);
					DATA(12) <= s_shift_reg(11);
					DATA(11) <= s_shift_reg(12);
					DATA(10) <= s_shift_reg(13);
					DATA(9) <= s_shift_reg(14);
					DATA(8) <= s_shift_reg(15);
					DATA(7) <= s_shift_reg(16);
					DATA(6) <= s_shift_reg(17);
					DATA(5) <= '0';
					DATA(4) <= '0';
					DATA(3) <= '0';
					DATA(2) <= '0';
					DATA(1) <= '0';
					DATA(0) <= '0';
					s_counter_enable <= '0';
				end if;
			end if;  -- (s_counter_enable = '1')
		
			-- sets the signal s_current_busy to allow the comparison of the last BUSY signal state 
			if(s_current_busy = '1' and BUSY = '0') then
				s_current_busy     <= '0';
				s_counter_enable   <= '1';
				s_counter          <=  0 ;
				s_shift_reg        <= "000000000000000000";
			elsif(s_current_busy = '0' and BUSY = '1') then
				s_current_busy     <= '1';			
			end if;  -- (s_current_busy = '1' and BUSY = '0')

		end if; -- reset / falling_edge
	end process ltc2378;
	
	-- process to reset the DATA_RDY signal 
	reset_DATA_RDY: process(RESET, CLK_50MHz, s_rdy, s_counter)
	begin
	   if(RESET = '1') then
	       DATA_RDY         <= '0';
	       s_rdy            <= '0';
	   elsif(CLK_50MHz'event and CLK_50MHz = '1') then
	       -- sets DATA_RDY to '0' at last transfered BIT 
	       if(s_counter = (width-1)) then
               s_rdy            <= '0';
           end if;
            -- resets the DATA_RDY to '1' after copying shift_reg to DATA
            if(s_rdy = '0') then
                s_rdy           <= '1';
            end if;
            DATA_RDY <= s_rdy;
        end if; -- reset / rising_edge
	end process reset_DATA_RDY;
	

    -- enables the clk to shift the data out of the ADC
	copy_clk: process(s_clk_enable, CLK_50MHz)
	begin
		if(s_clk_enable = '1') then 
			SCK_out <= CLK_50MHz;
		end if; 
	end process copy_clk;


	CNV_out <= CNV_in;
	
end rtl;
